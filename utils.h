/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H
#define UTILS_H

#define ALIGNMENT ((size_t)64)

#define IS_POW2(_n) ((_n) != 0) && (((_n) & ((_n) - 1)) == 0)
#define RUP_POW2(_n, _p) (((_n) + (_p) - 1) & ~((_p) - 1))
#define RDN_POW2(_n, _p) ((_n) & ~((_p) - 1))

#endif /* UTILS_H */
