/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>

#include "utils.h"

static size_t page_size = 4096;

size_t
query_page_size(void)
{
	return sysconf(_SC_PAGE_SIZE);
}

void
page_alloc_init(void)
{
	page_size = query_page_size();
}

bool
page_alloc(size_t count, Pages *pages)
{
	int fd = open("/dev/zero", O_RDWR);
	if (fd < 0) {
		perror("open");
		return false;
	}

	void *map = mmap(NULL, count * page_size,
			PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
	close(fd);
	if (map == MAP_FAILED) {
		perror("mmap");
		return false;
	}

	pages->data = map;
	pages->size = count * page_size;

	return true;
}

void
page_free(Pages *pages)
{
	assert(munmap(pages->data, pages->size) == 0);
}
