/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#define WIN32_LEAN_AND_MEAN
#define NOGDICAPMASKS
#define NOVIRTUALKEYCODES
#define NOWINMESSAGES
#define NOWINSTYLES
#define NOSYSMETRICS
#define NOMENUS
#define NOICONS
#define NOKEYSTATES
#define NOSYSCOMMANDS
#define NORASTEROPS
#define NOSHOWWINDOW
#define OEMRESOURCE
#define NOATOM
#define NOCLIPBOARD
#define NOCOLOR
#define NOCTLMGR
#define NODRAWTEXT
#define NOGDI
#define NOKERNEL
#define NONLS
#define NOMB
#define NOMEMMGR
#define NOMETAFILE
#define NOMINMAX
#define NOOPENFILE
#define NOSCROLL
#define NOSERVICE
#define NOSOUND
#define NOTEXTMETRIC
#define NOWH
#define NOWINOFFSETS
#define NOCOMM
#define NOKANJI
#define NOHELP
#define NOPROFILER
#define NODEFERWINDOWPOS
#define NOMCX
#include <windows.h>

#include "utils.h"

static size_t page_size = 4096;

size_t
query_page_size(void)
{
	SYSTEM_INFO si;
	GetSystemInfo(&si);
	return si.dwPageSize;
}

void
page_alloc_init(void)
{
	page_size = query_page_size();
}

static void
win32_perr(char *fmt, ...)
{
	fputs("allocators: \x1b[01;31merror: \x1b[0m", stderr);

	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);

	DWORD err_code = GetLastError();
	if (err_code == ERROR_SUCCESS) {
		fputc('\n', stderr);
		return;
	}

	LPTSTR buf;
	DWORD ret = FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, err_code,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&buf, 0, NULL);
	if (ret > 0) {
		/* Windows adds a newline to the end of buf. */
		fprintf(stderr, ": %s", buf);
		LocalFree(buf);
	} else {
		fputc('\n', stderr);
	}
}

bool
page_alloc(size_t count, Pages *pages)
{
	size_t size = page_size * count;

	HANDLE map = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL,
			PAGE_READWRITE, 0, size, NULL);
	if (!map) {
		win32_perr("Failed to map %zu pages", count);
		return false;
	}

	LPVOID addr = MapViewOfFile(map, FILE_MAP_READ | FILE_MAP_WRITE,
			0, 0, size);
	CloseHandle(map);
	if (!addr) {
		win32_perr("Failed to map view of file");
		return false;
	}

	pages->data = addr;
	pages->size = size;

	return true;
}

void
page_free(Pages *pages)
{
	assert(UnmapViewOfFile(pages->data));
}
