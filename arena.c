/*
 * Copyright (C) 2023, 2024  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "utils.h"
#include "arena.h"

void
arena_alloc_init(const Pages *const pages, Arena *arena)
{
	arena->start = (void *)RUP_POW2((uintptr_t)pages->data, ALIGNMENT);
	arena->curr = arena->start;
	arena->size = RDN_POW2(pages->size, ALIGNMENT);
}

static inline bool
is_within_bounds(Arena *arena, void *alloc_start, size_t alloc_size)
{
	return
		((uintptr_t)alloc_start + alloc_size) <=
		((uintptr_t)arena->start + arena->size);
}

void *
arena_alloc(Arena *arena, size_t size)
{
	if (size == 0)
		return NULL;
	size = RUP_POW2(size, ALIGNMENT);

	if (!is_within_bounds(arena, arena->curr, size))
		return NULL;

	void *ptr = arena->curr;
	assert((uintptr_t)ptr % ALIGNMENT == 0 && "Alignment is off");

	arena->curr = (void *)((uintptr_t)arena->curr + size);

	return ptr;
}

void
arena_clear(Arena *arena)
{
	arena->curr = arena->start;
}
