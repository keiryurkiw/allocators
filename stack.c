/*
 * Copyright (C) 2023, 2024  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "utils.h"
#include "stack.h"

/*
 * Alignment is stored in the block header which is stored
 * right before the beginning of each block.
 */
static const uint16_t padding = ALIGNMENT - sizeof(padding);

void
stack_alloc_init(const Pages *const pages, Stack *stack)
{
	stack->start = (void *)RUP_POW2((uintptr_t)pages->data, ALIGNMENT);
	stack->curr = stack->start;
	stack->size = pages->size;
}

static inline bool
is_within_bounds(Stack *stack, void *alloc_start, size_t alloc_size)
{
	return
		((uintptr_t)alloc_start + alloc_size) <=
		((uintptr_t)stack->start + stack->size);
}

void *
stack_alloc(Stack *stack, size_t size)
{
	if (size == 0)
		return NULL;
	size = RUP_POW2(size, ALIGNMENT);

	void *ptr = (void *)((uintptr_t)stack->curr + ALIGNMENT);
	if (!is_within_bounds(stack, ptr, size))
		return NULL;

	/* Insert block header (amount of padding). */
	uint16_t *padding_ptr = (uint16_t *)((uintptr_t)ptr - sizeof(uint16_t));
	*padding_ptr = padding;

	stack->curr = (void *)((uintptr_t)stack->curr + size);

	return ptr;
}

void
stack_free(Stack *stack, void *ptr)
{
	uint16_t *padding_ptr = (uint16_t *)((uintptr_t)ptr - sizeof(uint16_t));
	ptr = (void *)((uintptr_t)ptr - (*padding_ptr + sizeof(uint16_t)));

	stack->curr = (void *)((uintptr_t)stack->start - (uintptr_t)ptr);
}

void
stack_clear(Stack *stack)
{
	stack->curr = stack->start;
}
