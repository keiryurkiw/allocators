/*
 * Copyright (C) 2023, 2024  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "stack.h"
#include "platform/utils.h"
#include "utils.h"

int
main(void)
{
	page_alloc_init();

	Pages pages;
	if (!page_alloc(4, &pages))
		return 1;

	Stack stack;
	stack_alloc_init(&pages, &stack);

	char str[] = "Hello, world!";
	void *ptr = stack_alloc(&stack, sizeof(str));
	memcpy(ptr, str, sizeof(str));
	puts(str);

	stack_clear(&stack);

	page_free(&pages);

	return 0;
}
